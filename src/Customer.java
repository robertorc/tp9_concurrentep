
import java.util.Random;


public class Customer implements Runnable {
	
	static final int WAITING_TIME_MIN = 2000; //This must be randomly while the customers are waiting for somebody else
	
	static final int DECIDE_TIME_MIN = 20000;
	static final int DECIDE_TIME_MAX = 30000;
	static final int FRIST_DECIDE_TIME_MIN = 1000;
	static final int FIRST_DECIDE_TIME_MAX = 10000;

			
	
	
	String id = new String("UNDEF");
	static private Random randomGenerator = new Random();
	
	public Customer(String id){
		this.id = id;
	}
	
	public void run(){
		boolean firstTimeCustomer = true;
		//boolean lastCustomer = true;
		while(true){
			try{
				if(firstTimeCustomer){	
					Thread.sleep(FRIST_DECIDE_TIME_MIN + 
	                        randomGenerator.nextInt(FIRST_DECIDE_TIME_MAX - FRIST_DECIDE_TIME_MIN));    	
				} else{
					Thread.sleep(DECIDE_TIME_MIN + 
	                        randomGenerator.nextInt(DECIDE_TIME_MAX - DECIDE_TIME_MIN));
				}
                if(RollerCoaster.monitor.board(this) == 1){
                	firstTimeCustomer = false;
                } else{
                	firstTimeCustomer = true;
                }
        		
			} catch (InterruptedException e) {
                e.printStackTrace();
            }
		
		
		
		}
		
	}
	
	
}
