
import java.util.Random;



public class Train implements Runnable{
	
	static final int DURATION_TRIP_MIN = 5000;
	static final int DURATION_TRIP_MAX = 6000;
	static final int TRAIN_PREPARATION_TIME = 1000;
	
	static private Random randomGenerator = new Random();	
	String id = new String("UNDEF");
	
	public Train(String id){
		this.id = id;
	}
	
	
	public void run(){
		while(true){
			try{
				
				RollerCoaster.monitor.load();
				
				Thread.sleep(DURATION_TRIP_MIN + 
 					randomGenerator.nextInt(DURATION_TRIP_MAX - DURATION_TRIP_MIN));
				RollerCoaster.monitor.unload();
				
				System.out.println("The train preparing time");
				Thread.sleep(TRAIN_PREPARATION_TIME);
				
			}
			catch (InterruptedException e) {
                e.printStackTrace();
            }
			
		}
	}

}
