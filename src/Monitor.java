

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;;


 

public class Monitor {
	
	final Lock lock = new ReentrantLock();
	final Condition train_ready =  lock.newCondition(); //When the train has all passenger aboard
	final Condition train_departing = lock.newCondition();//This condition should me called after train_ready is signaled
	final Condition train_finished = lock.newCondition(); //Condition after the train arrives to the station
	final Condition train_empty = lock.newCondition(); //The train does not have any passenger by the moment
		
	//The train cannot depart if the customer is not ready	
	final Condition customer_ready = lock.newCondition(); 
	final Condition customer_leaving = lock.newCondition();
	final Condition customer_traveling = lock.newCondition();
	final Condition customer_arrived = lock.newCondition();
	
	
	final static int SEATS = 6;
	
	int customersWaiting = 0;
	int customersOnBoard =0;
	int seatsAvailable = 0;
	
	boolean calledLoad = false;
	boolean calledUnload = false;
	
	boolean customersBoarding = false;
	boolean customersTraveling = false;
	
	
	
	

//*********************Methods called by the Train*************************************//
	
	public int load() throws InterruptedException{
		int customerNumber;
		lock.lock();
		try{
			calledLoad = true;
			System.out.println("The train checks for customers");
			while((customersWaiting <= 0)){ //Make wait cust. until the train have 6 passengers
				System.out.println("The train is waiting in the station");
				train_empty.await();
			}
			customerNumber = customersWaiting; //Checks for the last passenger to board
			train_ready.await();//The train is now full with customers to roll, waiting for customer to be ready
			
			System.out.println("---Train depparting---> ");
			train_departing.signal();
			
			//train_finished.signal();
		} finally{
			lock.unlock();
		}
		
		return customerNumber;
	}
	
	public void unload() throws InterruptedException{	
		lock.lock();
		try{
			if(!customersTraveling){
			System.out.println("<--- Train finished --");
				train_finished.signal();
				
			}
			calledUnload = true;
		} finally{
			lock.unlock();
		}
		
	}
	
	
//**********************Methods called by the Customer**********************************//
	//This methods Could receive the Object Customer
	public int board(Customer c) throws InterruptedException{
		int status = -1;
		
		lock.lock();
		try{
			if(calledLoad){
				System.out.println(c.id + " wants a ride");	//Checks for any customer to ride the train
				if((customersWaiting < SEATS) && ((!customersTraveling))){
					customersWaiting++;
					
					train_empty.signal(); //It is not empty no more
					
					System.out.println("> " + c.id +" boarding" );
					train_departing.await(); 
					customersTraveling = true;
					customersOnBoard = customersWaiting;
										
					
					//The train is in course
					//train_finished.signal();
					customersTraveling = false;
					return 1;
					
				} else{
					System.out.println("Train is full"); //Check for the last to enter
					train_ready.signal();
				
					if(customersTraveling){
						System.out.println(c.id + " waiting for the train to come");
					}
				return 0;
				}				
			}
			calledLoad = false;
			return status;
			
			
		}finally{
			lock.unlock();
		}		
	}
	
	public int unboard(Customer c) throws InterruptedException{
			lock.lock();
				try{
					if(calledUnload){	
						train_finished.await();
						System.out.println(c.id + "wants to go out");
						while((customersOnBoard <= 0)&& (!customersTraveling)){
						customersOnBoard--;
						System.out.println(c.id + " is uboarding ");
						}
					return 1;
					}
					return 0;
				} finally{
				lock.unlock();
				}	
	}
//*********************************Boolean Methods**********************************//
	/*
	public boolean checkSeats() throws InterruptedException{
		if((seatsAvailable > 0)  && (!customersTraveling)){
			seatsAvailable--;
			return true;
		}
		return false;
	}
	public boolean checkRidingCustomers() throws InterruptedException{
		if(seatsAvailable == 0){
			seatsAvailable = SEATS;
			customersTraveling = true;
			return true;			
		}
		return false;
	}
	*/
//**********************************End Boolean Methods****************************//

}
