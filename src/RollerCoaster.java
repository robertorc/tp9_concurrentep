
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 
 * RollerCoaster main class
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos-Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @author 		Damiano Zoppi <Damiano.Zoppi@edu.hefr.ch>
 * @version     1.5          
 * @since       2012-05-29
 *
 * 
 *
 */

public class RollerCoaster {
	public static ExecutorService threadExecutor = Executors.newCachedThreadPool();
	public static Monitor monitor = new Monitor();
	
	final static int NUMBER_CLIENTS_PARK = 20;
	final static int NUMBER_CLIENTS_TRAIN = 6;
	
	public static void main(String []args){
		String name;
	    File inFile = new File("names.txt");
	    Scanner in;
		try{
			in = new Scanner(inFile);
			Train train = new Train("TRAIN");
			Customer customers[] = new Customer[NUMBER_CLIENTS_PARK];
			threadExecutor.execute(train);
			for (int i = 0; i < NUMBER_CLIENTS_PARK; i++) {
                name = in.nextLine();
                customers[i] = new Customer(name);
                threadExecutor.execute(customers[i]);
            }
		} catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		
	}


}
